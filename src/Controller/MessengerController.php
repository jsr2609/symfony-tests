<?php

namespace App\Controller;

use App\Message\SmsNotification;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class MessengerController extends AbstractController
{
    #[Route('/messenger', name: 'app_messenger')]
    public function index(MessageBusInterface $bus): Response
    {
        $bus->dispatch(new SmsNotification('Look! I created a message.'));

        return $this->render('messenger/index.html.twig', [
            'controller_name' => 'MessengerController',
        ]);
    }
}
