<div id="modal-edit-<?= $entity_class_name ?>" class="modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Edit <?= $entity_class_name ?>
          <small class="m-0 text-muted">
            ...
          </small>
        </h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <ul class="list-inline">
        
        <li class="list-inline-item">
            {{ include('<?= $templates_path ?>/_delete_form.html.twig', {'entity_name': '<?= $entity_class_name ?>'}) }}
        </li>
    </ul>

    {{ include('<?= $templates_path ?>/_form.html.twig', {'button_label': 'Actualizar', 'entity_name': '<?= strtolower($entity_class_name) ?>'}) }}
      </div>
      <div class="modal-footer">
        <button id="btn-update-<?= strtolower($entity_class_name) ?>" class="btn btn-primary m-btn">
        <span>
            <i class="fal fa-save"></i>
            <span>{{ button_label|default('Save')}}</span>
        </span>
    </button>
        
        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fal fa-times-circle"></i> Cancelar</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
    $('#modal-edit-<?= $entity_class_name ?>').modal('show');
    swalConfirmation =  helpers.createConfirmation();
    deleteForm = $("#form-delete-<?= $entity_class_name ?>");
    deleteForm.submit( function(e)
    {
      e.preventDefault();
      $('#btn-delete-<?= strtolower($entity_class_name) ?>').prop('disabled', true);
      swalConfirmation.fire().then(function(result) {
        if (result.value)
        {
          e.target.submit();
        }
      });

    });
    btnUpdate = $("#btn-update-<?= strtolower($entity_class_name) ?>");

    btnUpdate.on("click", () => {
      form.submit();
    })
    form = $("form[name={{ form.vars.name }}]");

    form.validate({
    });
    form.on("submit", function (e) {
        e.preventDefault();
        url = $(this).attr('action');
        formData = new FormData(this);

        if(form.valid()) {
            $.ajax({
            url: url,
            dataType: "JSON",
            type: "POST",
            data: formData,
            processData: false,
            contentType: false,
            success: function (data, textStatus, jqXHR) {
              helpers.createNotification('success', data.message);
              $(location).attr('href',data.url_redirect);
            },
            error: function (qXHR, textStatus, errorThrow) {
                const message = qXHR.status+' '+qXHR.responseJSON.message;
                helpers.createNotification('error', message);
                //un//blockPage();
            },
            beforeSend: function( xhr ) {
                btnUpdate.prop("disabled", true);
                helpers.blockUI();
            },
            complete: function( jqXHR, textStatus ) {
                btnUpdate.prop("disabled", false);
                helpers.unblockUI();
            }
        });
        }
    });
</script>
