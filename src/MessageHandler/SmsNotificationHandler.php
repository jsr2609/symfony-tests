<?php

declare(strict_types=1);

namespace App\MessageHandler;

use App\Message\SmsNotification;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class SmsNotificationHandler implements MessageHandlerInterface
{

    public function __invoke(SmsNotification $message)
    {
        //Realizar algo
    }
}
