<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixtures extends Fixture
{
    private UserPasswordHasherInterface $hasher;

    public function __construct(UserPasswordHasherInterface $hasher)
    {
        $this->hasher = $hasher;
    }

    public function load(ObjectManager $manager): void
    {
        // $product = new Product();
        // $manager->persist($product);
        $user = new User();
        $user->setNombre('ADMINISTRADOR');
        $user->setEmail('admin@gmail.com');
        $user->setRoles(['ROLE_ADMIN', 'ROLE_BACKEND']);
        $user->setApellidos('DEL SISTEMA');
        $password = $this->hasher->hashPassword($user, 'xswedcxs');
        $user->setPassword($password);

        $manager->persist($user);

        $manager->flush();
    }
}
