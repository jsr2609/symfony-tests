<?php

namespace App\Test\Controller;

use App\Entity\Prueba;
use App\Repository\PruebaRepository;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PruebaControllerTest extends WebTestCase
{
    private KernelBrowser $client;
    private PruebaRepository $repository;
    private string $path = '/prueba/';

    protected function setUp(): void
    {
        //Solo agregué comentarios innecesarios
        //Add otro comentario
        
        $this->client = static::createClient();
        $this->repository = (static::getContainer()->get('doctrine'))->getRepository(Prueba::class);

        foreach ($this->repository->findAll() as $object) {
            $this->repository->remove($object, true);
        }
    }

    public function testIndex(): void
    {
        $crawler = $this->client->request('GET', $this->path);
        //Comentario para verificar el array_merge
        //Agregue otro comentario perro
        //Otra ves perro

        self::assertResponseStatusCodeSame(500);
        

        // Use the $crawler to perform additional assertions e.g.
        // self::assertSame('Some text on the page', $crawler->filter('.p')->first());
    }

    public function testNew(): void
    {
        $originalNumObjectsInRepository = count($this->repository->findAll());

        
        $this->client->request('GET', sprintf('%snew', $this->path));

        self::assertResponseStatusCodeSame(200);

        $this->client->submitForm('Save', [
            'prueba[nombre]' => 'Testing',
        ]);

        self::assertResponseRedirects('/prueba/');

        self::assertSame($originalNumObjectsInRepository + 1, count($this->repository->findAll()));
    }

    
}
